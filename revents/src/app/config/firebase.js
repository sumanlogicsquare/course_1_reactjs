import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/compat/database";
import "firebase/compat/auth";
import "firebase/compat/storage";

const firebaseConfig = {
    apiKey: "AIzaSyCEYo4DnUUVEjUR6vqWEwoyX3bUxNBIQXo",
    authDomain: "reventscourse-9f33a.firebaseapp.com",
    projectId: "reventscourse-9f33a",
    storageBucket: "reventscourse-9f33a.appspot.com",
    messagingSenderId: "76070772733",
    appId: "1:76070772733:web:6a0ad8fcef86a84e4e0394"
};


firebase.initializeApp(firebaseConfig);
firebase.firestore();

export default firebase;
