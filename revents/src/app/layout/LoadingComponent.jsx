import React from "react";
import { Spinner } from "react-bootstrap";
// import { Loader, Dimmer } from "semantic-ui-react";

export default function LoadingComponent({
    inverted = true,
    content = "Loading...!",
}) {
    return (
        <>
            {/* <Dimmer inverted={inverted} active={true}>
                <Loader content={content} />
            </Dimmer> */}
            <Spinner
                animation="grow"
                variant="success"
                style={{
                    margin: "auto",
                    padding: "10px 0 0 30px",
                    fontSize: "80px",
                }}
                inverted={inverted}
            >
                <span className="visually-hidden" style={{ fontSize: "60px" }}>
                    {content}
                </span>
            </Spinner>
        </>
    );
}

// Component is not usable
