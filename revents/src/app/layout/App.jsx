import React from "react";
import { Route, Switch } from "react-router-dom";
import EventDashboard from "../../features/events/eventDashboard/EventDashboard";
import NavBar from "../../features/nav/NavBar";
import HomePage from "../../features/home/HomePage";
import EventForm from "../../features/events/eventForm/EventForm";
import EventDetailedPage from "../../features/events/eventDetailed/EventDetailedPage";
import ErrorPage from "./ErrorPage";
import Sandbox from "../../features/sandbox/Sandbox";
import { useLocation } from "react-router-dom/cjs/react-router-dom.min";
import ModalManager from "../common/modals/ModalManager";
import { ToastContainer } from "react-toastify";

function App() {
    const { key } = useLocation();
    return (
        <>
            <ModalManager />
            <ToastContainer
                theme="colored"
                position="bottom-right"
                // hideProgressBar
            />
            <Route exact path="/" component={HomePage} />

            <Route
                path={"/(.+)"}
                render={() => (
                    <>
                        <NavBar />
                        <Switch>
                            <Route
                                exact
                                path="/events"
                                component={EventDashboard}
                            />
                            <Route
                                exact
                                path="/events/:id"
                                component={EventDetailedPage}
                            />
                            <Route
                                exact
                                path={["/createEvent", "/manage/:id"]}
                                component={EventForm}
                                key={key}
                            />
                            <Route
                                exact
                                path={["/createEvent", "/sandbox"]}
                                component={Sandbox}
                            />
                            <Route component={ErrorPage} />
                        </Switch>
                    </>
                )}
            />
        </>
    );
}

export default App;

// <EventDashboard
//                 formOpen={formOpen}
//                 setFormOpen={setFormOpen}
//                 selectEvent={handleSelectEvent}
//                 selectedEvent={selectedEvent}
//             />
