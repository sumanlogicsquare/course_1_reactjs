import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { asyncActionFinish, asyncActionStart, asyncActionError } from "../../app/async/asyncReducer";
import { dataFromSnapshot } from "../firestore/firestoreService";


export default function useFirestoreCollection({query, data, deps}){
    const dispatch = useDispatch();

    useEffect(() => {
        // dispatch(asyncActionStart());
        const unsubscribe = query().onSnapshot(
            snapshot => {
                const docs = snapshot.docs.map(doc => dataFromSnapshot(doc));
                data(docs);
                // dispatch(asyncActionFinish());
            },
            error => dispatch(asyncActionError())
        )
        return () => unsubscribe();
    }, deps) // eslint-disable-line react-hooks/exhaustive-deps
}


// useEffect(() => {
    // const unsubscribe = getEventsFromFirestore({
        // next: snapshot => {
            // dispatch(listenToEvents(snapshot.docs.map(docSnapshot => dataFromSnapshot(docSnapshot))))
        // },
        // error: error => console.log(error),
        // complete: () => console.log("Can not be seen message!"),
    // })
    // return unsubscribe;
// }, [dispatch])