import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { asyncActionFinish, asyncActionStart, asyncActionError } from "../../app/async/asyncReducer";
import { dataFromSnapshot } from "../firestore/firestoreService";


export default function useFirestoreDoc({query, data, deps}){
    const dispatch = useDispatch();

    useEffect(() => {
        // dispatch(asyncActionStart());
        const unsubscribe = query().onSnapshot(
            snapshot => {
                data(dataFromSnapshot(snapshot));
                // dispatch(asyncActionFinish());
            },
            error => dispatch(asyncActionError())
        )
        return () => unsubscribe();
    }, deps) // eslint-disable-line react-hooks/exhaustive-deps
}


// useEffect(() => {
    // const unsubscribe = getEventsFromFirestore({
        // next: snapshot => {
            // dispatch(listenToEvents(snapshot.docs.map(docSnapshot => dataFromSnapshot(docSnapshot))))
        // },
        // error: error => console.log(error),
        // complete: () => console.log("Can not be seen message!"),
    // })
    // return unsubscribe;
// }, [dispatch])