import React from 'react';
import { useField } from 'formik';

const MySelectInput = ({label, ...props}) => {
    const [field, meta, helpers] = useField(props);

    return(
        <form error = {meta.touched && !!meta.error}>
            <label>{label}</label>
            {/* <textarea {...field} {...props} /> */}

            <select name="languages" id="lang" multiple value={field.value || null} >
                {/* <option value="javascript">JavaScript</option>
                <option value="php">PHP</option> */}
            </select>

            { meta.touched && meta.error ? (
                <p style={{color:"red"}}>{meta.error}</p>
            ) : null }
        </form>
    )
}

export default MySelectInput;