import React from 'react';
import { useField } from 'formik';

const MyTextInput = ({label, ...props}) => {
    const [field, meta] = useField(props);

    return(
        <form error = {meta.touched && !!meta.error}>
            <label>{label}</label>
            <input {...field} {...props} />
            { meta.touched && meta.error ? (
                <p style={{color:"red"}}>{meta.error}</p>
            ) : null }
        </form>
    )
}

export default MyTextInput;