import React from 'react';
import { useField } from 'formik';

const MyTextArea = ({label, ...props}) => {
    const [field, meta] = useField(props);

    return(
        <form error = {meta.touched && !!meta.error}>
            <label>{label}</label>
            <textarea {...field} {...props} />
            { meta.touched && meta.error ? (
                <p style={{color:"red"}}>{meta.error}</p>
            ) : null }
        </form>
    )
}

export default MyTextArea;