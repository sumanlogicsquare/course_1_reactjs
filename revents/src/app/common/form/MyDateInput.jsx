import React from 'react';
import { useField, useFormikContext } from 'formik';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const MyDateInput = ({label, ...props}) => {
    const {setFieldValue} = useFormikContext();
    const [field, meta] = useField(props);

    return(
        <form error = {meta.touched && !!meta.error}>
            <label>{label}</label>
            <DatePicker
                {...field}
                {...props}
                selected={(field.value && new Date(field.value)) || null }
                onChange={value => setFieldValue(field.name, value)}
            />

            { meta.touched && meta.error ? (
                <p style={{color:"red"}}>{meta.error}</p>
            ) : null }
        </form>
    )
}

export default MyDateInput;