import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../app/common/modals/modalReducer";
import { decrement, increment } from "./testReducer";

const Sandbox = () => {
    const dispatch = useDispatch();
    const value = useSelector((state) => state.test.data);
    const [target, setTarget] = useState(null); //for loading spinner
    // const { loading } = useSelector((state) => state.aync); //for loading spinner but its not showing
    // console.log(value);
    return (
        <>
            <h1>Testing Sandbox</h1>
            <h2>The value is : {value}</h2>
            <button
                // loading={loading && target === "increment"}
                onClick={(e) => {
                    dispatch(increment(10));
                    // setTarget(e.target.name);
                }}
            >
                +
            </button>
            <button
                // loading={loading && target === "decrement"}
                onClick={(e) => {
                    dispatch(decrement(5));
                    // setTarget(e.target.name);
                }}
            >
                -
            </button>
            <button
                onClick={() =>
                    dispatch(
                        openModal({
                            modalType: "TestModal",
                            modalProps: { value },
                        })
                    )
                }
            >
                Open Modal
            </button>
        </>
    );
};

export default Sandbox;
