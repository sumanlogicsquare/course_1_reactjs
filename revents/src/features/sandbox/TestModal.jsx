import React from "react";
import ModalWrapper from "../../app/common/modals/ModalWrapper";

const TestModal = ({ value }) => {
    return (
        <ModalWrapper size="mini" header="Test Modal">
            <div>The data is: &nbsp;{value}</div>
        </ModalWrapper>
    );
};

export default TestModal;
