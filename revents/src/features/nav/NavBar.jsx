import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import SignedInMenu from "./SignedInMenu";
import SignedOutMenu from "./SignedOutMenu";
// import { NavLink } from "react-router-dom/cjs/react-router-dom.min";

// import image from "./assets/imageLogo.png";

const NavBar = () => {
    const { authenticated } = useSelector((state) => state.auth);

    // const {setFormOpen} = props;

    return (
        <nav className="navUl">
            <ul>
                <li>
                    <NavLink EXACT to={"/"}>
                        <img
                            src="/assets/imageLogo.png"
                            className="active"
                            alt="..."
                        ></img>
                    </NavLink>
                </li>
                {/* <li><a href="#news">News</a></li> */}
                <li>
                    <a>
                        <NavLink as={NavLink} to={"/events"}>
                            Events
                        </NavLink>
                    </a>
                </li>
                <li>
                    <a>
                        <NavLink as={NavLink} to={"/sandbox"}>
                            Sandbox
                        </NavLink>
                    </a>
                </li>
                {/* <li><a href="#about">About</a></li> */}
                {authenticated && (
                    <NavLink
                        as={NavLink}
                        to={"/createEvent"}
                        className="buttonCls button1"
                    >
                        Create Event
                    </NavLink>
                )}
                {/* <button onClick={() => setFormOpen(true)} className="buttonCls button1"  >Create Event</button> */}
                {authenticated ? <SignedInMenu /> : <SignedOutMenu />}
            </ul>
        </nav>
    );
};

export default NavBar;
