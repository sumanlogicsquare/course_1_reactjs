import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { signOutUser } from "../auth/authActions";

const SignedInMenu = () => {
    const dispatch = useDispatch();
    const { currentUser } = useSelector((state) => state.auth);
    const history = useHistory();
    return (
        <>
            <image
                src={currentUser.photoURL || "/assets/user.png"}
                alt="..."
            ></image>
            <div className="dropdown">
                <span>{currentUser.email}</span>
                <div className="dropdown-content">
                    <p>
                        <Link to={"/createEvent"}>Create Event</Link>
                    </p>
                    <p>User Profile</p>
                    <p
                        onClick={() => {
                            dispatch(signOutUser());
                            history.push("/");
                        }}
                    >
                        Log Out
                    </p>
                </div>
            </div>
        </>
    );
};

export default SignedInMenu;
