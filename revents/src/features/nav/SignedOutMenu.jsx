import React from "react";
import { useDispatch } from "react-redux";
import { openModal } from "../../app/common/modals/modalReducer";

const SignedOutMenu = ({ setAuthenticated }) => {
    const dispatch = useDispatch();
    return (
        <>
            <button
                onClick={() => dispatch(openModal({ modalType: "LoginForm" }))}
                className="buttonCls button2"
            >
                LogIn
            </button>
            <button className="buttonCls button2">Register</button>
        </>
    );
};

export default SignedOutMenu;
