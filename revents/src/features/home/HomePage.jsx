import React from 'react';

const HomePage = ({history}) => {
    return(
        <>
            <h1 style={{ textAlign:"center" }}>WELCOME TO RE-VENTS</h1><br /><br />
            <button onClick={() => history.push("/events")} className="buttonCls button3" >GET STARTED &nbsp; <span className="arrowSpan">&#8669;</span>  </button>
        </>
    )
}



export default HomePage;