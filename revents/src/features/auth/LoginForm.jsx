import React from "react";
import ModalWrapper from "../../app/common/modals/ModalWrapper";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import MyTextInput from "../../app/common/form/MyTextInput";
import { useDispatch } from "react-redux";
import { signInUser } from "./authActions";
import { closeModal } from "../../app/common/modals/modalReducer";

const LoginForm = () => {
    const dispatch = useDispatch();

    return (
        <ModalWrapper size="mini" header="SignIn to Re-Vents">
            <Formik
                initialValues={{ email: "", password: "" }}
                validationSchema={Yup.object({
                    email: Yup.string()
                        .required()
                        .email(),
                    password: Yup.string().required(),
                })}
                onSubmit={(values, { setSubmitting }) => {
                    console.log(values);
                    dispatch(signInUser(values));
                    setSubmitting(false);
                    dispatch(closeModal());
                }}
            >
                {({ isSubmitting, isValid, dirty }) => (
                    <Form style={{ marginBottom: "180px" }}>
                        <MyTextInput
                            name="email"
                            placeholder="Email Address"
                            className="loginInputCls"
                        />
                        <MyTextInput
                            name="password"
                            placeholder="Password"
                            type="password"
                            className="loginInputCls"
                        />
                        <button
                            className="eventFormSubmit"
                            type="submit"
                            loading={isSubmitting}
                            disabled={!isValid || !dirty || isSubmitting}
                        >
                            LogIn
                        </button>
                    </Form>
                )}
            </Formik>
        </ModalWrapper>
    );
};

export default LoginForm;
