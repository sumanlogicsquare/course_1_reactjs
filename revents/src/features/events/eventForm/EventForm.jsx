import React from "react";
import cuid from "cuid";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createEvent, updateEvent } from "../eventActions";
import { Formik, Form } from 'formik';
import * as Yup from "yup";
import MyTextInput from "../../../app/common/form/MyTextInput";
import MyTextArea from "../../../app/common/form/MyTextArea";
import MySelectInput from "../../../app/common/form/MySelectInput";
import { categoryData } from "../../../app/api/categoryOptions";
import MyDateInput from "../../../app/common/form/MyDateInput";


const EventForm = ({match, history}) => {
    const dispatch = useDispatch();
    const selectedEvent = useSelector((state) =>
        state.event.events.find((e) => e.id === match.params.id)
    );


    const initialValues = selectedEvent
        ? selectedEvent
        : {
              title: "",
              category: "",
              description: "",
              city: "",
              venue: "",
              date: "",
          };

    
    const validationSchema = Yup.object({
        title: Yup.string().required("You must provide a Title"),
        category: Yup.string().required("You must provide a Category"),
        description: Yup.string().required("You must provide a Description"),
        city: Yup.string().required("You must provide a City"),
        venue: Yup.string().required("You must provide a Venue"),
        date: Yup.string().required("You must provide a Date"),
    })




    return (
        <div
            style={{
                backgroundColor: "rgba(255, 255, 255, 0.8)",
                borderRadius: "25px",
                padding: "15px",
            }}
        >
            

            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => {

                    selectedEvent
                        ? dispatch(updateEvent({ ...selectedEvent, ...values }))
                        : dispatch(createEvent({
                            ...values,
                            id: cuid(),
                            hostedBy: "Bob",
                            attendees: [],
                            hostPhotoURL: "./assets/user.png",
                        }));
                        console.log(values);
                        history.push("/events");
                    }}>
        
                
                {({isSubmitting, dirty, isValid}) => (
                    <Form className="eventForm" style={{pading: "250px"}}>
                    {/* <div style={{pading: "250px"}}>
                        <label>Event Title</label>
                        <Field className="eventInputCls" name="title" placeholder="Event Title"/>
                        <ErrorMessage name="title" render={error => <p style={{color: "red"}}>{error}</p> } />
                    </div> */}

                <h2 style={{ textAlign: "center", color:"teal" }}>
                    Event Details
                </h2>

                    <div>
                        <label>Event Title</label>
                        <MyTextInput className="eventInputCls" name="title" placeHolder="Event Title"/>
                    </div>

                    {/* <div>
                        <label>Event Category</label>
                        <MySelectInput className="eventInputCls" name="category" placeholder="Event Categiry" options={categoryData} />
                    </div> */}
                    <div>
                        <label>Event Category</label>
                        <MyTextInput className="eventInputCls" name="category" placeholder="Event Categiry(Eg: Culture, Drinks and Trvel)" options={categoryData} />
                    </div>

                    <div>
                        <label>Event Description</label>
                        <MyTextArea className="eventInputCls" name="description" placeholder="Event Description"/>
                    </div>
                    <h2 style={{ textAlign: "center", color:"teal" }}>
                        Event Location Details
                    </h2>
                    <div>
                        <label>Event City</label>
                        <MyTextInput className="eventInputCls" name="city" placeholder="Event City"/>
                    </div>
                    <div>
                        <label>Event Venue</label>
                        <MyTextInput className="eventInputCls" name="venue" placeholder="Event Venue"/>
                    </div>
                    <div>
                        <label>Event Date</label>
                        <MyDateInput 
                        className="eventInputClsDt" 
                        name="date" 
                        placeholderText="Event Date" 
                        timeFormat="HH:MM"
                        showTimeSelect="true"
                        timeCaption="time"
                        dateFormat="MMMM d, yyyy h:mm a"
                        />
                    </div>


                    <button className="eventFormSubmit" 
                    type="submit"
                    loading={isSubmitting}
                    disabled={!isValid || !dirty || isSubmitting}
                    >
                        Submit
                    </button>
                    <Link to="/events">
                        <button
                            className="eventFormCancel"
                            type="submit"
                            disabled={isSubmitting}
                            value="Cancel"
                        >
                            Cancel
                        </button>
                    </Link>
                </Form>
                )}

                
            </Formik>
        </div>
    );
};

export default EventForm;





{/* 

                    <div>
                        <label>Category</label>
                        <input
                            type="text"
                            id="category"
                            name="category"
                            value={values.category}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Description</label>
                        <input
                            type="text"
                            id="description"
                            name="description"
                            value={values.description}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>City</label>
                        <input
                            type="text"
                            id="city"
                            name="city"
                            value={values.city}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Venue</label>
                        <input
                            type="text"
                            id="venue"
                            name="venue"
                            value={values.venue}
                            onChange={handleChange}
                        />
                    </div>
                    <div>
                        <label>Date</label>
                        <input
                            type="date"
                            id="date"
                            name="date"
                            value={values.date}
                            onChange={handleChange}
                        />
                    </div> */}