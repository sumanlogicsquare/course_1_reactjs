import React from "react";
import { Segment, Placeholder } from "semantic-ui-react";
export default function EventListItemPlaceholder() {
    return (
        <Placeholder
            fluid
            style={{ border: "1px solid black", marginBottom: "10px" }}
        >
            <Segment.Group>
                <Segment style={{ minHeight: 110 }}>
                    <Placeholder>
                        <Placeholder.Header image>
                            <Placeholder.Line />
                            <Placeholder.Line />
                        </Placeholder.Header>
                        <Placeholder.Paragraph>
                            <Placeholder.Line />
                        </Placeholder.Paragraph>
                    </Placeholder>
                </Segment>
                <Segment>
                    <Placeholder>
                        <Placeholder.Line />
                        <Placeholder.Line />
                    </Placeholder>
                </Segment>
                <Segment style={{ minHeight: 70 }} />
                <Segment>
                    <button
                        style={{
                            background: "rgb(34, 190, 214)",
                            marginLeft: "10px",
                        }}
                    >
                        View
                    </button>
                </Segment>
            </Segment.Group>
        </Placeholder>
    );
}
