import React from "react";

const EventListAttendee = ({attendee}) => {
    return(
        <ul style={{listStyle: "none"}}>
            <li>
                <img style={{height: "45px", width: "auto"}} src={attendee.photoURL} alt="" srcset="" />
            </li>
        </ul>
    )
}

export default EventListAttendee;