import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import EventListAttendee from "./EventListAttendee";
import { deleteEvent } from "../eventActions";
import {format} from "date-fns";


const EventListItem = (props) => {
    const { event } = props;
    const dispatch = useDispatch();

    const {
        hostPhotoURL,
        date,
        venue,
        title,
        hostedBy,
        description,
        attendees,
    } = event;

    return (
        <div
            style={{
                backgroundColor: "cyan",
                marginBottom: "50px",
                borderRadius: "25px",
            }}
        >
            <table>
                <tr>
                    <td style={{ padding: "20px" }}>
                        <img src={hostPhotoURL} alt="..." />
                        <href className="">#View</href>
                        <href className="">#Save</href>
                        <href className="">#Share</href>
                    </td>
                    <td style={{ padding: "20px", width: "75%" }}>
                        <span className="">Date: {format(date, "MMMM d, yyyy h:mm a")}</span> <br />
                        <span className="">Place: {venue}</span>
                        <h2 className="">{title}</h2>
                        <span className="">{hostedBy}</span>
                        <p>Image Title</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div
                            style={{
                                paddingLeft: "20px",
                                textAlign: "justify",
                                width: "250%",
                            }}
                        >
                            {description}
                        </div>
                    </td>
                    <td>
                        <Link to={`/events/${event.id}`}>
                            <button
                                className="btnListItem"
                                style={{ float: "right" }}
                            >
                                View
                            </button>
                        </Link>

                        <button
                            onClick={() => dispatch(deleteEvent(event.id))}
                            className="btnListItem"
                            style={{ float: "right" }}
                        >
                            Delete
                        </button>
                    </td>
                </tr>
            </table>
            <ul
                style={{
                    backgroundColor: "lightgray",
                    innerWidth: "100%",
                    paddingTop: "5px",
                    paddingBottom: "2px",
                    display: "flex",
                    listStyle: "none",
                }}
            >
                {attendees.map((attendee) => (
                    <li>
                        <EventListAttendee
                            key={attendee.id}
                            attendee={attendee}
                        />
                    </li>
                ))}
                {/* <li>
                    <EventListAttendee />
                </li>
                <li>
                    <EventListAttendee />
                </li> */}
            </ul>
        </div>
    );
};

export default EventListItem;
