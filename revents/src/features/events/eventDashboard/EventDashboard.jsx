import React from "react";
import EventList from "../eventList/EventList";

// import { sampleData } from "../../../app/api/sampleData";
import { useDispatch, useSelector } from "react-redux";
import LoadingComponent from "../../../app/layout/LoadingComponent";
import EventFilters from "./EventFilters";
import { listenToEventsFromFirestore } from "../../../app/firestore/firestoreService";
import { listenToEvents } from "../eventActions";
// import { asyncActionfinish, asyncActionStart, asyncActionError } from "../../../app/async/asyncReducer";
// import EventListItemPlaceholder from "../eventList/EventListItemPlaceholder";
import useFirestoreCollection from "../../../app/hooks/useFirestoreCollection";


const EventDashboard = (props) => { 
    const dispatch = useDispatch();
    const { events } = useSelector((state) => state.event);
    const { loading } = useSelector((state) => state.async);
    if (loading) return <LoadingComponent />; // Loading Indecator Component

    useFirestoreCollection({
        query: () => listenToEventsFromFirestore(),
        data: events => dispatch(listenToEvents(events)),
        deps: [dispatch],
    });


    // useEffect(() => {
    //     const unsubscribe = listenToEventsFromFirestore({
    //         next: snapshot => {
    //             dispatch(listenToEvents(snapshot.docs.map(docSnapshot => dataFromSnapshot(docSnapshot))))
    //         },
    //         error: error => console.log(error),
    //         complete: () => console.log("Can not be seen message!"),
    //     })
    //     return unsubscribe;
    // }, [dispatch])

    // useEffect(() => {
    //     dispatch(asyncActionStart())
    //     const unsubscribe = getEventsFromFirestore({
    //         next: snapshot => {
    //             dispatch(listenToEvents(snapshot.docs.map(docSnapshot => dataFromSnapshot(docSnapshot))))
    //             dispatch(asyncActionfinish());
    //         },
    //         error: error => dispatch(asyncActionError(error)),
    //         complete: () => console.log("Can not be seen message!"),
    //     })
    //     return unsubscribe;
    // }, [dispatch])

    return (
        <div className="grid-container">
            <div className="item1">
                {loading && (
                    <>
                        {/* <EventListItemPlaceholder />
                        <EventListItemPlaceholder /> */}
                    </>
                )}
                <EventList events={events} />
            </div>
            <div className="item2">
                <EventFilters />
            </div>
        </div>
    );
};

export default EventDashboard;
