import React from "react";
import Calendar from "react-calendar";

const EventFilters = () => {
    return (
        <div
            style={{
                border: "1px solid black",
                padding: "10px 10px 20px 10px",
                marginBottom: "20px",
                backgroundColor: "white",
                borderRadius: "25px",
            }}
        >
            <div vertical size="large">
                <h2
                    icon="filter"
                    style={{
                        background: "lightBlue",
                        borderRadius: "15px",
                        textAlign: "center",
                    }}
                >
                    Filter
                </h2>
                <p>All Event</p>
                <p>I'm going</p>
                <p>I'm hoisting</p>
            </div>
            <h2
                icon="calender"
                style={{
                    background: "lightBlue",
                    borderRadius: "15px",
                    textAlign: "center",
                }}
            >
                Selected Date
            </h2>
            <Calendar />
        </div>
    );
};

export default EventFilters;
