import React from "react";

const EventDetailedSidebar = ({ attendees }) => {
    return (
        <>
            <p
                style={{
                    backgroundColor: "aliceblue",
                    fontSize: "30px",
                    margin: "0px",
                }}
            >
                <span>{attendees.length}</span>{" "}
                {attendees.length > 1 ? "pepole are" : "person"} Going
            </p>
            <div className="grid-container-row3">
                {attendees.map((attendee) => (
                    <div key={attendee.id}>
                        <img
                            className="imgChat"
                            src={attendee.photoURL || "/assets/user.png"}
                            alt="..."
                        />
                        &nbsp;&nbsp;
                        <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                            {attendee.displayName}
                        </span>
                    </div>
                ))}
                {/* <div>
                    <img className="imgChat" src="/assets/user.png" alt="..." />
                    &nbsp;&nbsp;
                    <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                        Matt
                    </span>
                </div> */}
            </div>
        </>
    );
};

export default EventDetailedSidebar;
