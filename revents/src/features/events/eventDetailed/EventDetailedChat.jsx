import React from "react";

const EventDetailedChat = () => {
    return (
        <>
            <div className="grid-container-row2">
                <p
                    style={{
                        backgroundColor: "aliceblue",
                        fontSize: "30px",
                        margin: "0px",
                    }}
                >
                    Chat About This Event
                </p>
                <div>
                    &nbsp;
                    <p>
                        Date : <span>&nbsp;</span>
                    </p>
                    <img className="imgChat" src="/assets/user.png" alt="..." />
                    &nbsp;&nbsp;
                    <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                        Matt
                    </span>
                    <p>This is comment section</p>
                    <input type="submit" value="Reply" />
                </div>
                <div>
                    &nbsp;
                    <p>
                        Date : <span>&nbsp;</span>
                    </p>
                    <img className="imgChat" src="/assets/user.png" alt="..." />
                    &nbsp;&nbsp;
                    <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                        Matt
                    </span>
                    <p>This is comment section</p>
                    <input type="submit" value="Reply" />
                    <div style={{ padding: "40px" }}>
                        &nbsp;
                        <p>
                            Date : <span>&nbsp;</span>
                        </p>
                        <img
                            className="imgChat"
                            src="/assets/user.png"
                            alt="..."
                        />
                        &nbsp;&nbsp;
                        <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                            Matt
                        </span>
                        <p>This is comment section</p>
                        <input type="submit" value="Reply" />
                    </div>
                </div>
                <div>
                    &nbsp;
                    <p>
                        Date : <span>&nbsp;</span>
                    </p>
                    <img className="imgChat" src="/assets/user.png" alt="..." />
                    &nbsp;&nbsp;
                    <span style={{ fontSize: "30px", fontWeight: "bold" }}>
                        Matt
                    </span>
                    <p>This is comment section</p>
                    <input type="submit" value="Reply" />
                </div>
                <form action="">
                    <div className="row">
                        <div className="col-75">
                            <textarea
                                className="txtArea"
                                id="subject"
                                name="subject"
                                placeholder="Write something.."
                                style={{ height: "200px" }}
                            ></textarea>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <button className="button11" type="submit">
                            Add Review
                        </button>
                    </div>
                </form>
            </div>
        </>
    );
};

export default EventDetailedChat;
