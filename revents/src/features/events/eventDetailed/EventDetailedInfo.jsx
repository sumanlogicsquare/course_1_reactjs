import React from "react";
import {format} from "date-fns";

const EventDetailedInfo = ({ event }) => {
    return (
        <>
            <div className="grid-container-row">
                <div>
                    Event Description >>> &nbsp;{" "}
                    <span>{event.description}</span>
                </div>
                <div>
                    Event Date >>> &nbsp; <span>{format(event.date, "MMMM d, yyyy h:mm a")}</span>
                </div>
                <div>
                    Event Venue >>> &nbsp; <span>{event.venue}</span>
                </div>
            </div>
            <a href="#" className="button11">
                Show Map
            </a>
        </>
    );
};

export default EventDetailedInfo;
