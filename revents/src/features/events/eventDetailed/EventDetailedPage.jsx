import React from "react";
import EventDetailedHeader from "./EventDetailedHeader";
import EventDetailedInfo from "./EventDetailedInfo";
import EventDetailedChat from "./EventDetailedChat";
import EventDetailedSidebar from "./EventDetailedSidebar";
import { useSelector } from "react-redux";
import useFirestoreDoc from "../../../app/hooks/useFirestoreDoc";
import { listenToEventFromFirestore } from "../../../app/firestore/firestoreService";
import {listenToEvents} from "../eventActions";

const EventDetailedPage = ({ match }) => {
    const event = useSelector((state) =>
        state.event.events.find((e) => e.id === match.params.id)
    );

    useFirestoreDoc({
        query: () => listenToEventFromFirestore(match.params.id),
        data: event => dispatchEvent(listenToEvents([event])),
    })

    return (
        <>
            <div className="grid-container1">
                <div className="item11">
                    <EventDetailedHeader event={event} />
                </div>
                <div className="item22">
                    <EventDetailedInfo event={event} />
                </div>
                <div className="item33">
                    <EventDetailedChat />
                </div>
                <div className="item44">
                    <EventDetailedSidebar attendees={event.attendees} />
                </div>
            </div>
        </>
    );
};

export default EventDetailedPage;
