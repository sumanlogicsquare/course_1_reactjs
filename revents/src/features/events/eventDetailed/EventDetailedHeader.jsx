import React from "react";
import { Link } from "react-router-dom";
import {format} from "date-fns";

const EventDetailedHeader = ({ event }) => {
    return (
        <>
            <div className="card1">
                <img
                    src={`/assets/categoryImages/${event.category}.jpg`}
                    className="card1__image"
                />
                <div className="card1__overlay">
                    <div className="overlay1__text">
                        <h1>{event.title}</h1>
                        <p>
                            Event Date: &nbsp; <span>{format(event.date, "MMMM d, yyyy h:mm a")}</span>
                        </p>
                        <h2>Host by {event.hostedBy}</h2>
                    </div>
                </div>
            </div>
            <a href="#" className="button111">
                Cancel My Place
            </a>
            <a href="#" className="button112">
                Join The Event
            </a>
            <Link to={`/manage/${event.id}`}>
                <a href="#" className="button11">
                    Manage Event
                </a>
            </Link>
        </>
    );
};

export default EventDetailedHeader;
