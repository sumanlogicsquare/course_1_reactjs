import React from 'react';
import ReactDOM from 'react-dom';
import './app/layout/styles.css';
import 'react-toastify/dist/ReactToastify.min.css';
import "react-calendar/dist/Calendar.css";
import App from './app/layout/App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './app/store/configureStore';
import ScrollToTop from './app/layout/ScrollToTop';
// import { loadEvents } from './features/events/eventActions';


const store = configureStore();
// console.log(store.getState());

// store.dispatch(loadEvents());

const root = ReactDOM.createRoot(document.getElementById('root'));

const renderElement = () => {
  root.render(
    <Provider store={store}>
      <BrowserRouter>
      <ScrollToTop />
        <App />
      </BrowserRouter>
    </Provider>
  );
}


// const root = document.getElementById('root');

// const renderElement = () => {
//   ReactDOM.render(<App />, root);
// }


if(module.hot){
  module.hot.accept("./app/layout/App", () => {
    setTimeout(renderElement);
  })
}


renderElement();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
